KeysetMonsterPlay = KeysetMonsterPlay or {};

local VERSION_SETTINGS = 1;

local isLibSlashRegistered = false;
local isLibAddonButtonRegistered = false;
local localization = KeysetMonsterPlay.Localization.GetMapping();

local function RegisterLibs()
	if (not isLibSlashRegistered) then
		if (LibSlash) then
			LibSlash.RegisterWSlashCmd("keysetmonsterplay", function(args) KeysetMonsterPlay.SlashCommand(args) end);
			isLibSlashRegistered = true;
		end
	end

	if (not isLibAddonButtonRegistered) then
		if (LibAddonButton) then
			Keyset.AddMenuModule(L"Monster Play", KeysetMonsterPlay.Setup.Profile.Show);
			isLibAddonButtonRegistered = true;
		end
	end
end

function KeysetMonsterPlay.Initialize()
	RegisterEventHandler(SystemData.Events.LOADING_END, "KeysetMonsterPlay.OnLoadingEnd");
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "KeysetMonsterPlay.OnLoadingEnd");
    RegisterEventHandler(SystemData.Events.PLAY_AS_MONSTER_STATUS, "KeysetMonsterPlay.HandlePlayAsMonsterStatus");
	
	KeysetMonsterPlay.LoadSettings();
	
	KeysetMonsterPlay.Setup.Profile.Initialize();
end

local function UpdateSettings()
	local settings = KeysetMonsterPlay.Settings;
	local version = settings.Version;

	if (version == 1) then
		version = 2;
	end
end

function KeysetMonsterPlay.LoadSettings()
	if (not KeysetMonsterPlay.Settings) then
		KeysetMonsterPlay.Settings = {};
	else
		UpdateSettings();
	end
	
	local settings = KeysetMonsterPlay.Settings;
	settings.Version = VERSION_SETTINGS;

	if (not settings.Profile) then
		settings.Profile = {};
	end
end

function KeysetMonsterPlay.SlashCommand(args)
	local option, value = args:match(L"([a-z0-9]+)[ ]?(.*)");
	
	if (type(option) == "wstring") then
		option = option:lower();
	end

	KeysetMonsterPlay.Setup.Profile.Show();
end

function KeysetMonsterPlay.OnLoadingEnd()
	RegisterLibs();
end

function KeysetMonsterPlay.HandlePlayAsMonsterStatus(isPlayAsMonster)
	local settings = KeysetMonsterPlay.Settings;
	if (not settings.Enabled) then return end
	
	if (isPlayAsMonster) then
		if (settings.Profile.MonsterPlay) then
			Keyset.LoadProfile(settings.Profile.MonsterPlay);
		end
	else
		if (settings.Profile.Default) then
			Keyset.LoadProfile(settings.Profile.Default);
		end
	end
end