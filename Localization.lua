KeysetMonsterPlay = KeysetMonsterPlay or {};
if (not KeysetMonsterPlay.Localization) then
	KeysetMonsterPlay.Localization = {};
	KeysetMonsterPlay.Localization.Language = {};
end

local localizationWarning = false;

function KeysetMonsterPlay.Localization.GetMapping()

	local lang = KeysetMonsterPlay.Localization.Language[SystemData.Settings.Language.active];
	
	if (not lang) then
		if (not localizationWarning) then
			d("Your current language is not supported. English will be used instead.");
			localizationWarning = true;
		end
		lang = KeysetMonsterPlay.Localization.Language[SystemData.Settings.Language.ENGLISH];
	end
	
	return lang;
	
end