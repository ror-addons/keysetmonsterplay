<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="KeysetMonsterPlay" version="1.0.1" date="12/14/2010" >		
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />		
    <Author name="Healix" email="" />		
    <Description text="Switches keybinds when the player plays as a monster (Skaven)" />        		
    <Dependencies>			
      <Dependency name="Keyset" />		
    </Dependencies>		
    <Files>			
      <File name="Localization.lua" />			
      <File name="Localization/enUS.lua" />			 			
      <File name="Core.lua" />			 			
      <File name="Setup/Profile.lua" />			
      <File name="Setup/Profile.xml" />		
    </Files>		 		
    <SavedVariables>			
      <SavedVariable name="KeysetMonsterPlay.Settings" />		   		
    </SavedVariables>		 		
    <OnInitialize>			
      <CreateWindow name="KeysetMonsterPlayProfileWindow" show="false" />			
      <CallFunction name="KeysetMonsterPlay.Initialize" />		
    </OnInitialize>		
    <OnUpdate />		
    <OnShutdown />		
    <WARInfo />	
  </UiMod>
</ModuleFile>