KeysetMonsterPlay = KeysetMonsterPlay or {};
if (not KeysetMonsterPlay.Localization) then
	KeysetMonsterPlay.Localization = {};
	KeysetMonsterPlay.Localization.Language = {};
end

KeysetMonsterPlay.Localization.Language[SystemData.Settings.Language.ENGLISH] = 
{
	["Setup.Profile.Title"] = L"Monster Play Keybindings",
	["Setup.Profile.Enable"] = L"Enable automatic keybind switching",
	["Setup.Profile.DefaultProfile"] = L"Select a default profile to use",
	["Setup.Profile.MonsterProfile"] = L"Select a profile to use when playing as a monster",
	["Setup.Profile.None"] = L"None",
};

