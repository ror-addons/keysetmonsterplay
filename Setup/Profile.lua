KeysetMonsterPlay = KeysetMonsterPlay or {};
KeysetMonsterPlay.Setup = KeysetMonsterPlay.Setup or {};
KeysetMonsterPlay.Setup.Profile =
{
	WindowName = "KeysetMonsterPlayProfileWindow",
};

local windowName = KeysetMonsterPlay.Setup.Profile.WindowName;
local localization = KeysetMonsterPlay.Localization.GetMapping();
local availableProfiles = {};
local lockSettings = false;

local function LoadProfilesCombo(comboName, selectedValue)
	local defaultIndex = 1;
	ComboBoxClearMenuItems(comboName);
	ComboBoxAddMenuItem(comboName, localization["Setup.Profile.None"]);

	for index, name in ipairs(availableProfiles) do
		ComboBoxAddMenuItem(comboName, towstring(name));
		if (selectedValue and name == selectedValue) then
			defaultIndex = index + 1;
		end
	end
	
	ComboBoxSetSelectedMenuItem(comboName, defaultIndex);
end

local function LoadProfiles()
	lockSettings = true;
	availableProfiles = Keyset.GetProfiles();
	local settings = KeysetMonsterPlay.Settings;
	LoadProfilesCombo(windowName .. "DefaultProfileComboBox", settings.Profile.Default);
	LoadProfilesCombo(windowName .. "MonsterProfileComboBox", settings.Profile.MonsterPlay);
	lockSettings = false;
end

function KeysetMonsterPlay.Setup.Profile.Initialize()

	LabelSetText(windowName .. "TitleLabel", localization["Setup.Profile.Title"]);
	LabelSetText(windowName .. "EnableCheckboxLabel", localization["Setup.Profile.Enable"]);
	LabelSetText(windowName .. "DefaultProfileLabel", localization["Setup.Profile.DefaultProfile"]);
	LabelSetText(windowName .. "MonsterProfileLabel", localization["Setup.Profile.MonsterProfile"]);

	KeysetMonsterPlay.Setup.Profile.LoadSettings();

end

function KeysetMonsterPlay.Setup.Profile.LoadSettings()

	local settings = KeysetMonsterPlay.Settings;
	ButtonSetPressedFlag(windowName .. "Enable" .. "Button", (settings.Enabled == true));

end

function KeysetMonsterPlay.Setup.Profile.Show()

	if (WindowGetShowing(windowName)) then return end
	
	LoadProfiles();	
	WindowSetShowing(windowName, true);
	
	WindowUtils.AddToOpenList(windowName, KeysetMonsterPlay.Setup.Profile.OnCloseLUp, WindowUtils.Cascade.MODE_NONE);
	Sound.Play(Sound.WINDOW_OPEN);

end

function KeysetMonsterPlay.Setup.Profile.Hide()

	if (not WindowGetShowing(windowName)) then return end
	
	WindowSetShowing(windowName, false);
	
	Sound.Play(Sound.WINDOW_CLOSE);
	WindowUtils.RemoveFromOpenList(windowName);

end

function KeysetMonsterPlay.Setup.Profile.OnCloseLUp()

	KeysetMonsterPlay.Setup.Profile.Hide();

end

function KeysetMonsterPlay.Setup.Profile.OnHidden()
	
end

function KeysetMonsterPlay.Setup.Profile.OnEnableLUp()
	local isChecked = ButtonGetPressedFlag(windowName .. "Enable" .. "Button") == false;
	ButtonSetPressedFlag(windowName .. "Enable" .. "Button", isChecked);
	
	KeysetMonsterPlay.Settings.Enabled = isChecked;
end

function KeysetMonsterPlay.Setup.Profile.OnDefaultProfileChanged()
	if (lockSettings) then return end
	local value = ComboBoxGetSelectedMenuItem(windowName .. "DefaultProfileComboBox");
	local profile = nil;
	if (value > 1) then
		profile = availableProfiles[value - 1];
	end
	KeysetMonsterPlay.Settings.Profile.Default = profile;
end

function KeysetMonsterPlay.Setup.Profile.OnMonsterProfileChanged()
	if (lockSettings) then return end
	local value = ComboBoxGetSelectedMenuItem(windowName .. "MonsterProfileComboBox");
	local profile = nil;
	if (value > 1) then
		profile = availableProfiles[value - 1];
	end
	KeysetMonsterPlay.Settings.Profile.MonsterPlay = profile;
end